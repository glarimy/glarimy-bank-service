# Cloud Services
Spring Boot with Cloud, Dockers and Kubernetes

Login to ``https://labs.play-with-docker.com/`` with your docker-hub id.

Start a Linux instance

Clone the code repo on to the instance

```
git clone https://glarimy@bitbucket.org/glarimy/glarimy-bank-service.git
```

Build the docker image

```
docker build -t glarimy/glarimy-bank .
```

Login to docker-hub and export the image

```
docker login
docker push glarimy/glarimy-bank
```

Get the images from docker-hub

```
docker pull mysql
docker pull glarimy/glarimy-bank
```

Create a network

```
docker network create glarimy-nw
docker network ls
```

Run the docker containers

```
docker container run --name mysqldb --network glarimy-nw -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=glarimy -d mysql
docker container logs -f mysql-db
docker container run --network glarimy-nw --name glarimy-bank -p 8080:8080 -d glarimy/glarimy-bank
docker container exec -it mysql-db bash
```

Verify the apps

```
curl -X POST -H 'Content-Type: application/json' --data '{"name":"Krishna", "phone":9731423166}' http://localhost:8080/account
curl http://localhost:8080/account/1
```

Run the docker compose

```
docker-compose up
```

### Minikube ###

Get Hold of a Kubernetes Environment

```
https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/
https://www.katacoda.com/courses/kubernetes
```

Start MiniKube (Single Node Cluster)

```
minikube start
```

Verify the nodes, pods, services and deployments

```
kubectl get nodes
kubectl get pods
kubectl get services
kubectl get deployments
kubectl get rs
```

Deploy and expose MySQL

```
kubectl create -f mysql-deployment.yml
kubectl expose deployment mysqldb --port 3306 --type=LoadBalancer

```

Expose and expose Bank API

```
kubectl create -f bank-deployment.yml
kubectl expose deployment glarimy-bank --port 8080 --type=LoadBalancer
```

Verify the nodes, pods, services and deployments

```
kubectl get nodes
kubectl get pods
kubectl get services
kubectl get deployments
kubectl get rs
kubectl describe pods
kubectl describe services
```

Use the service

```
curl external-ip:8080/account/1
curl -X POST -H 'Content-Type: application/json'  --data '{"name":"Krishna", "phone":9731423166}' external-ip:nodeport/account
curl external-ip:8080/account/1
```

### Smaller Tasks ###

Launch pod with MySQL

``` 
kubectl create -f mysql-pod.yml
kubectl get pods
kubectl describe pods
kubectl logs mysqldb
kubectl exec -it mysqldb bash
```

Expose MySQL as a service

```
kubectl expose pod mysqldb --port=3306 --name=mysqldb --type=NodePort
kubectl get services
kubectl describe services
```

Launch pod with bank

```
kubectl create -f bank-pod.yml
kubectl get pods
kubectl describe pods
kubectl logs bank
```

Expose bank as a service

```
kubectl expose pod bank --port=8080 --name=bank --type=NodePort
kubectl get services
kubectl describe services
```

Access REST services locally

```
minikube ip
kubectl get services
curl <minikube ip>:nodePort/account/1
curl -X POST -H 'Content-Type: application/json'  --data '{"name":"Krishna", "phone":9731423166}' minikubeob:nodeport/account
curl <minikube ip>:nodePort/account/1
```

Access REST services externally

```
http://<dns>/account/1
```